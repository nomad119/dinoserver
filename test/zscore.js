const chai = require('chai')
const chaiHttp = require('chai-http')
const should = chai.should()
const server = require('../index')
chai.use(chaiHttp)

describe('score calls', () => {
  afterEach(() => {
    server.close()
  })
  it('retrieves top 5 scores', (done) => {
    chai.request(server)
      .get("/api/user/scores")
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a("object");
        done()
      })
  })
})