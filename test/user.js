const chai = require('chai')
const chaiHttp = require('chai-http')
const should = chai.should()
const server = require('../index')
let gen_user_email = `test.${Date.now()}@gmail.com`
let jwtToken = ''
chai.use(chaiHttp)

describe('user calls', () => {
  afterEach(() => {
    server.close()
  })
  it('signs user up', (done) => {
      chai.request(server)
        .post("/api/signup")
        .send({
          email: gen_user_email,
          password: "12345",
          score: 1500
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.property("token");
          done()
        })
    }),
    it('signs user in', (done) => {
      chai.request(server)
        .post("/api/signin")
        .send({
          email: gen_user_email,
          password: "12345"
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.property("token");
          jwtToken = res.body.token;
          done()
        })
    }),
    it('updates users email and score', (done) => {
      chai.request(server)
        .put("/api/user/update")
        .set("authorization", jwtToken)
        .send({
          email: gen_user_email + "hello",
          password: "12345",
          score: 900
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.property("email");
          res.body.email.should.equal(gen_user_email + "hello")
          res.body.should.have.property("score");
          res.body.score.should.equal(900)
          done()
        })
    }),
    it('updates users with bigger high score', (done) => {
      chai.request(server)
        .post("/api/user/topscore")
        .set("authorization", jwtToken)
        .send({
          email: gen_user_email,
          password: "12345",
          newScore: 1700
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.nested.property("score");
          res.body.score.should.equal(1700)
          done()
        })
    }),
    it('updates users with smaller high score', (done) => {
      chai.request(server)
        .post("/api/user/topscore")
        .set("authorization", jwtToken)
        .send({
          email: gen_user_email,
          password: "12345",
          newScore: 1500
        })
        .end((err, res) => {
          console.log(res.body)
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.nested.property("score");
          res.body.score.should.equal('Previous score is higher')
          done()
        })
    }),
    it('gets users score', (done) => {
      chai.request(server)
        .get("/api/user/score")
        .set("authorization", jwtToken)
        .send({
          email: gen_user_email,
          password: "12345",
          score: 1700
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.nested.property("score");
          res.body.score.should.equal(1700)
          done()
        })
    }),
    it('deletes user', (done) => {
      chai.request(server)
        .delete("/api/user/delete")
        .set("authorization", jwtToken)
        .send({
          email: gen_user_email,
          password: "12345",
          score: 1700
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a("object");
          res.body.should.have.nested.property("score");
          res.body.score.should.equal(1700)
          done()
        })
    })
})