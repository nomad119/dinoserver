//main starting point of the application
var config = require('./config');
var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var app = express();
var router = require('./router');
var mongoose = require('mongoose');

mongoose.connect(config.MONGO_URL);
//App Setup
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json({
    type: '*/*'
}))
//app.use(bodyParser.json({
//   type: '*/*'
//}));
router(app);



//Server Setup
//check to see if there is a environment process port availbe or 3090
var port = process.env.PORT || 3091;
let server = module.exports = app.listen(port)
console.log('Server listening on:', port);