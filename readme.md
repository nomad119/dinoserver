#Getting Started  
##Prerequisites  
*  [MongoDB](https://www.mongodb.com/download-center/community "MongoDB Community Edition") installation with a data/db directory created  
*  [Node.js](https://nodejs.org/en/ "Node Homepage") installation >= 10.15.1  
*  [Postman](https://www.getpostman.com/downloads/ "Postman Download") installation  
##Knowledge requirements  
*  Javascript with ES6 syntax  
*  Promises  
*  Express.js  
*  Mocha.js  
*  Chai.js  
*  Mongoose.js  
*  Passport.js  
##Instructions  
*  Clone the repository  
*  Run 'npm install' Inside the server folder where the package.json exists   
*  Run mongod_batch.bat to run your mongodb instance   
*  Run 'npm run test'  
*  All of the tests should passed  
*  You should be go to go!  
*  Run 'npm run dev' to start the server.  
*  Run Postman and import the included Postman Collection and Environment  
*  Happy Coding!  
