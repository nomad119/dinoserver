var Auth = require("./controllers/auth");
var passportService = require("./services/passport");
var passport = require("passport");
var scoreCtrl = require("./controllers/scoring");
var userCrtl = require("./controllers/userCrtl");

var requireAuth = passport.authenticate("jwt", {
    session: false
});
var requireSignin = passport.authenticate("local", {
    session: false
});

module.exports = function (app) {
    app.get("/", function (req, res) {
        res.send({
            hi: "there"
        });
    });
    app.post("/api/signin", requireSignin, Auth.signin);
    app.post("/api/signup", Auth.signup);
    app.get("/api/user/score", requireAuth, (req, res) => {
        scoreCtrl
            .getScore(req.user.id)
            .then(userScore =>
                res.json({
                    user_score: userScore
                })
            )
            .catch(err =>
                res.json({
                    error: err
                })
            );
    });
    app.post("/api/user/topscore", requireAuth, (req, res) => {
        scoreCtrl
            .insertScore(req.user.id, req.body.newScore)
            .then(userScore => {
                res.json({
                    user_score: userScore
                });
            })
            .catch(err =>
                res.json({
                    error: err
                })
            );
    });
    app.get("/api/user/scores", (req, res) => {
        scoreCtrl
            .getTopScores(5)
            .then(topScores =>
                res.json({
                    topScores: topScores
                })
            )
            .catch(err =>
                res.json({
                    error: err
                })
            );
    });    
    app.get("/api/user/hi", (req, res) => {
        res.json({"hello":"world"})
    });
    app.put("/api/user/update", requireAuth, (req, res) => {
        _upUser = {
            "email": req.body.email,
            "score": req.body.score
        }
        userCrtl.updateUser(req.user.id, _upUser)
            .then(updatedUser => {
                res.json(
                    updatedUser
                )
            })
            .catch(err => {
                console.log("error updating: ", err)
                res.json({
                    "error": err.errmsg
                })
            })
    })
    app.delete("/api/user/delete", requireAuth, (req, res) => {
        userCrtl.deleteUser(req.user.id)
            .then(deletedUser => {
                res.json(
                    deletedUser
                )
            })
            .catch(err => {
                console.log("error deleting: ", err)
                res.json({
                    "error": err.errmsg
                })
            })
    })
};