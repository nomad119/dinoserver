var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
//Define User model
var userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true
    },
    password: String,
    score: Number
});

// On save Hook, encrypt password
// Before saving a model, run this function
userSchema.pre('save', function (next) {
    // get access to the user model
    var user = this;

    // generate a salt then run callback
    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
        }

        // hash (encrypt) our password using the salt
        bcrypt.hash(user.password, salt, null, function (err, hash) {
            if (err) {
                return next(err);
            }

            // overwrite plain text password with encrypted password
            user.password = hash;
            //next step user.save()
            next();
        });
    });
});

userSchema.methods.comparePassword = function (candiatePassword, callback) {
    bcrypt.compare(candiatePassword, this.password, function (err, isMatch) {
        if (err) {
            return callback(err);
        }

        callback(null, isMatch);
    });
};

//Create the model class
const ModelClass = mongoose.model('user', userSchema);
//Export the model
module.exports = ModelClass;