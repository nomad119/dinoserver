var User = require('../models/user');

/**
 * Deletes user by Id
 * @param  {Number} userId The user's Id
 * @return {Promise} returns deleted User
 */
exports.deleteUser = function (userId) {
  return new Promise((resolve, reject) => {
    User.findByIdAndRemove(userId, (err, results) => {
      if (err) reject(err)
      if (results) resolve(results)
    })
  })
}
/**
 * Updates User by Id
 * @param  {number} userId The user's Id
 * @param  {Object} updatedUser User changes 
 * @return {Promise} returns updated user
 */
exports.updateUser = function (userId, updatedUser) {
  return new Promise((resolve, reject) => {
    User.findById(userId, (err, user) => {
      if (updatedUser.email) user.email = updatedUser.email
      if (updatedUser.score) user.score = updatedUser.score
      user.save()
        .then(savedUser => resolve(savedUser))
        .catch(err => reject(err))
    })
  })
}