var config = require('../config');
var jwt = require('jwt-simple');
var User = require('../models/user');


//token generator for user
function tokenForUser(user) {
    var timestamp = new Date().getTime();
    return jwt.encode({
        sub: user.id,
        iat: timestamp
    }, config.secret);
}

exports.signup = function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    console.log('hi jacob', req.body)
    if (!email || !password) {
        return res.status(422).send({
            error: 'You must enter both email and password'
        });
    }
    //See if a user with the given email exists
    User.findOne({
        email: email
    }, function (err, existingUser) {
        if (err) {
            return next(err);
        }

        // If a user with email does exist, reutrn an error
        if (existingUser) {
            return res.status(422).send({
                error: 'Email is in use'
            });
        }

        //If a user with email does Not exist, create and save user record
        var user = new User({
            email: email,
            password: password
        });

        user.save(function (err) {
            if (err) {
                return next(err);
            }

            // Respond to requeste indicating the user was created
            res.json({
                token: tokenForUser(user)
            });
        });
    });

};

exports.signin = function (req, res, next) {
    //User has already had their email and password auth'd
    // we just to give them a token
    res.send({
        token: tokenForUser(req.user)
    });
};