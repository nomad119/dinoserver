var User = require('../models/user');
/**
 * Searches the user collection for the highest scores
 * @param  {Number} numOfScores The user's Id
 * @return {Object} returns the top score defined by numOfScores
 */
exports.getTopScores = function (numOfScores) {
  return User.aggregate().sort('field -score').limit(numOfScores).project({
    _id: 0,
    email: 1,
    score: 1
  })
}
/**
 * retrieves score from user
 * @param  {Number} userId The user's Id
 * @return {Promise} user's score
 */
const getScore = exports.getScore = function (userId) {
  return new Promise((resolve, reject) => {
    User.findById(userId, function (err, user) {
      if (err) {
        console.log(err)
        reject(err)
      }
      resolve(user.score)
    })
  })
}

/**
 * Checks the user's new score with the previous score.
 * If the score is highest the user's document is updated
 * @param  {Number} userId The user's Id
 * @param  {Number} newScore The user's new score
 * @return {Object} UpdatedUser Or
 * @return {Promise} the users older score is higher 
 */
exports.insertScore = function (userId, newScore) {
  return new Promise((resolve, reject) => {
    getScore(userId)
      .then(userOldScore => {
        if (newScore > userOldScore || userOldScore === undefined) {
          User.findById(userId, function (err, user) {
            if (err) reject(err)
            user.score = newScore
            user.save()
              .then(savedUser => resolve(savedUser.score))
              .catch(err => reject(err))
          })
        } else {
          resolve("Previous score is higher")
        }
      })
      .catch(err => reject(err))
  })
}