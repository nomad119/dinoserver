# Use node 6.3.0 LTF
# This image has node ready to go for us.
FROM node:10
# We create a new working directory that we can look inside later if something goes wrong with logging or something
WORKDIR /usr/src/dino-server
#Copy the package.json over into the workDIR
COPY package*.json ./
#Run npm install using the package.json
RUN npm install
#Copy over all the files to the work dir
COPY . .
#Open port 3091 which is where our server is going to run
EXPOSE 3091

CMD ["npm","start"] 