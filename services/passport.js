var passport = require('passport');
var User = require('../models/user');
var config = require('../config');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var LocalStrategy = require('passport-local');

// Setup options for JWT Strategy
var jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.secret
};
var localOptions = {
    usernameField: 'email'
};

// Create local strategy
var localLogin = new LocalStrategy(localOptions, function (email, password, done) {
    //Verify this email and password, call done with the user
    //if it is the correct email and apssword
    //otherwise, call done with false
    console.log('klasjdklfjaklsdjfklajsd;flj;aklsjf;klasjd;fl',email,password)
    User.findOne({
        email: email
    }, function (err, user) {
        if (err) {
            console.log('errrrrrrrrrrrrrrrr',err)
            return done(err);
        }

        if (!user) {
            console.log('errrrrrrrrrrrrrrrr',"oooppps")
            return done(null, false);
        }

        //compare passwords - is ' password' equal to user.password?
        user.comparePassword(password, function (err, isMatch) {
            console.log('asdkllkjasdklfj;lasjd',isMatch)
            if (err) {
                return done(err);
            }

            if (!isMatch) {
                return done(null, false);
            }

            return done(null, user);
        });
    });

});

// Create JWT strategy
var jwtLogin = new JwtStrategy(jwtOptions, function (payload, done) {
    // See if the user ID in the payload exists in our database
    // if it does, call 'done' with that user
    //otherwise, call done without a user object
    User.findById(payload.sub, function (err, user) {
        if (err) {
            return done(err, false);
        }

        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    });
});
// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);